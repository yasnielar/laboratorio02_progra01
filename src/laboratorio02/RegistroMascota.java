package laboratorio02;

import java.util.ArrayList;
import java.util.Scanner;

public class RegistroMascota {
    public static void leerMascotas() {
        Scanner sc = new Scanner(System.in);

        //Declaración de variables para leer los datos de los coches
        int IDMascota;
        String Nombre;
        String Genero;

        //Variable auxiliar que contendrá la referencia a cada coche nuevo.
        Mascota aux;
        int i, N;

        //se pide por teclado el número de coches a leer
        do {
            System.out.print("Número de Mascota? ");
            N = sc.nextInt();
        } while (N < 0);
        sc.nextLine(); //limpiar el intro

        //lectura de N coches
        for (i = 1; i <= N; i++) {
            //leer datos de cada coche
            System.out.println("Mascota " + i);
            System.out.print("ID Mascoa: ");
            IDMascota = sc.nextInt();
            System.out.print("Nombre: ");
            Nombre = sc.nextLine();
            System.out.print("Genero: ");
            Genero = sc.nextLine();
            System.out.print("Número de Kilómetros: ");
            sc.nextLine(); //limpiar el intro

            aux = new Mascota(); //Se crea un objeto Coche y se asigna su referencia a aux                          

            //se asignan valores a los atributos del nuevo objeto
            aux.setIDMascota(IDMascota);
            aux.setNombre(Nombre);
            aux.setGenero(Genero);

            //se añade el objeto al final del array
            Mascota.ListaMascota.add(aux);
           
        }
    }
}//fin método leerCoches()
