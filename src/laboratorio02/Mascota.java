/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio02;

import java.util.ArrayList;

/**
 *
 * @author yasni
 */
public class Mascota {

    private int IDMascota;
    private String Nombre;
    private String Genero;
    
    public static ArrayList<Mascota> ListaMascota = new ArrayList();

    public int getIDMascota() {
        return IDMascota;
    }

    public void setIDMascota(int IDMascota) {
        this.IDMascota = IDMascota;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getGenero() {
        return Genero;
    }

    public void setGenero(String Genero) {
        this.Genero = Genero;
    }

    public String toString() {
        StringBuilder ac = new StringBuilder();
        ac.append("\nID Mascota: ");
        ac.append(IDMascota);
        ac.append("\nNombre: ");
        ac.append(Nombre);
        ac.append("\nGenero: ");
        ac.append(Genero);
        return ac.toString();
    }

}
